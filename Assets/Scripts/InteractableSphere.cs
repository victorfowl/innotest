﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableSphere : MonoBehaviour
{
    //Editable configuration parameters.
    [Header("Interaction distance")]
    [SerializeField] private float actionDistance = 10;
    [Header("Time to close the dialog")]
    [SerializeField] private float responseSeconds = 4;
    [Header("Configuration serializable object")]
    [SerializeField] private CanvasConfig configuration;

    //private variables.
    private GameObject player, canvasPanel, maxiText;
    private Image textImage;
    private Text clickedText;
    private InputField inputText;
    private IEnumerator textCoroutine;
    private bool coroutineRuning;

    private void Awake()
    {
        //Spawn the canvas elements.
        SpawnElements();
    }

    private void Start()
    {
        //Take references.
        maxiText = GameObject.Find("MaximizePanelMode");
        canvasPanel = gameObject.transform.GetChild(0).gameObject;
        player = FindObjectOfType<PlayerMovement>().gameObject;        
    }

    void Update()
    {
        //Check if we are closest enougth to interact with the object
        if (Vector3.Distance(player.transform.position, transform.position) <= actionDistance)
        {
            if (!canvasPanel.activeSelf)
            {
                canvasPanel.SetActive(true);
            }
        }
        else
        {
            if (canvasPanel.activeSelf)
            {
                canvasPanel.SetActive(false);
            }
        }

        if (textImage.gameObject.activeSelf)
        {
            //Close dialog when we press scape.
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ClosePanel();
            }

            //Maximize the dialog text of the interactable object.
            if (Input.GetKeyDown(KeyCode.Return))
            {
                StopCoroutine(textCoroutine);
                maxiText.SetActive(true);
                if (clickedText)
                {
                    maxiText.GetComponentInChildren<Text>().text = clickedText.text;
                }
                if (inputText)
                {
                    maxiText.GetComponentInChildren<Text>().text = inputText.transform.GetChild(2).gameObject.GetComponent<Text>().text;
                }
                coroutineRuning = false;
            }
        }
    }

    //Debug and show the dialog when we right click on the object.
    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1) && Vector3.Distance(player.transform.position, transform.position) <= actionDistance)
        {
            Debug.Log("Hola mundo!");
            if (!textImage.gameObject.activeSelf)
            {
                player.GetComponent<PlayerMovement>().blockMovement = true;
                textImage.gameObject.SetActive(true);
                if (!coroutineRuning)
                {
                    textCoroutine = StartCount(responseSeconds);
                    StartCoroutine(textCoroutine);
                }
            }
        }
    }

    //Coroutine to close the dialog when time reaches the limit.
    private IEnumerator StartCount(float seconds) {
        coroutineRuning = true;
        yield return new WaitForSeconds(seconds);
        if (textImage.gameObject.activeSelf)
        {
            textImage.gameObject.SetActive(false);
        }
        player.GetComponent<PlayerMovement>().blockMovement = false;
        coroutineRuning = false;
    }

    //Close dialog function.
    public void ClosePanel()
    {
        textImage.gameObject.SetActive(false);
        StopCoroutine(textCoroutine);
        player.GetComponent<PlayerMovement>().blockMovement = false;
        maxiText.SetActive(false);
        maxiText.GetComponentInChildren<Text>().text = "";
        coroutineRuning = false;
    }
   
    //Spawn canvas elements function.
    private void SpawnElements() {

        //Only starts to spawn if there is a canvas in the config serializable object.
        if (configuration.sphereCanvas && configuration.clickedImage)
        {
            Canvas newCanvas = Instantiate(configuration.sphereCanvas, transform.TransformPoint(new Vector3(0, 1, 0)), transform.rotation, transform);
            textImage = Instantiate(configuration.clickedImage, newCanvas.transform.TransformPoint(Vector3.zero), newCanvas.transform.rotation, newCanvas.transform);
            if (configuration.closeButton)
            {
                Instantiate(configuration.closeButton, textImage.transform.TransformPoint(new Vector3(25, 40, 0)), textImage.transform.rotation, textImage.transform);
            }
            if (configuration.text)
            {
                clickedText = Instantiate(configuration.text, textImage.transform.TransformPoint(Vector3.zero), textImage.transform.rotation, textImage.transform);
                clickedText.text = configuration.textString;
            }
            if (configuration.inputField)
            {
                inputText = Instantiate(configuration.inputField, textImage.transform.TransformPoint(Vector3.zero), textImage.transform.rotation, textImage.transform);
            }
        }
    }
}
