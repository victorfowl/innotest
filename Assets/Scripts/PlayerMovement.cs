﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Editable configuration parameters.
    [Header("Speed")]
    [SerializeField] private float movementSpeed = 8;
    [Header("Rotation Speed")]
    [SerializeField] private float rotationSpeed = 40f;
    [Header("Tank control?")]
    [SerializeField] private bool tankControlMode = false;

    //Private variables.
    private Vector3 rot = Vector3.zero;
    private Animator anim;

    //Public variables.
    [HideInInspector] public bool blockMovement = false;


    // Use this for initialization
    void Awake()
    {
        //Initialize the rotation of the player.
        gameObject.transform.eulerAngles = rot;
        //Get animator reference.
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //Check if we can move
        if (!blockMovement)
        {
            //Check the pressed key and do something.
            CheckKey();
            //Rotate camera around the player.
            MoveCamera();
        }
    }

    void CheckKey() {

        if (Input.GetKey(KeyCode.W))
        {
            gameObject.transform.position = (gameObject.transform.position + (transform.forward * movementSpeed * Time.deltaTime));
            anim.SetBool("Walk_Anim", true);
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            anim.SetBool("Walk_Anim", false);
        }
        if (Input.GetKey(KeyCode.S))
        {
            gameObject.transform.position = (gameObject.transform.position + (transform.forward * -movementSpeed * Time.deltaTime));
            anim.SetFloat("Walk_Speed", -1);
            anim.SetBool("Walk_Anim", true);
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            anim.SetFloat("Walk_Speed", 1);
            anim.SetBool("Walk_Anim", false);
        }

        // Roll
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (anim.GetBool("Roll_Anim"))
            {
                anim.SetBool("Roll_Anim", false);
            }
            else
            {
                anim.SetBool("Roll_Anim", true);
            }
        }

        // Close
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (!anim.GetBool("Open_Anim"))
            {
                anim.SetBool("Open_Anim", true);
            }
            else
            {
                anim.SetBool("Open_Anim", false);
            }
        }

        //Check if we are in tank control mode
        if (!tankControlMode)
        {
            gameObject.transform.eulerAngles = rot;
            // Rotate Left
            if (Input.GetKey(KeyCode.A))
            {
                rot[1] -= rotationSpeed * Time.fixedDeltaTime;
            }

            // Rotate Right
            if (Input.GetKey(KeyCode.D))
            {
                rot[1] += rotationSpeed * Time.fixedDeltaTime;
            }
        }
        else
        {
            //If we are in tank mode we rotate the player and the camera with the right click pressed.
            if (Input.GetMouseButton(1))
            {
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y + Input.GetAxis("Mouse X") * 20, transform.localEulerAngles.z);
            }
        }
    }

    //Rotate camera function.
    void MoveCamera()
    {
        //Allways look to the player.
        Camera.main.transform.LookAt(transform);

        //Dont rotate if we aren't rightclicking or if we are in tank mode.
        if (Input.GetMouseButton(1) && !tankControlMode)
        {
            Camera.main.transform.parent.eulerAngles = new Vector3(Camera.main.transform.parent.eulerAngles.x, Camera.main.transform.parent.eulerAngles.y + Time.deltaTime * Input.GetAxis("Mouse X") * (rotationSpeed * 100), Camera.main.transform.parent.eulerAngles.z);
        }
    }
}
