﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpawnInteractables : EditorWindow
{
    private GameObject ground;
    private Object interactablePrefab;

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    private int numberOfObjects;
    static SpawnInteractables spawnWindow;

    [MenuItem("InnoTest/Spawn Random")]
    static void Init()
    {
        //Spawn a window.
        spawnWindow = (SpawnInteractables)EditorWindow.GetWindow(typeof(SpawnInteractables));
        spawnWindow.Show();
    }

    void OnGUI()
    {
        GUILayout.Label("Spawn Settings", EditorStyles.boldLabel);
        interactablePrefab = EditorGUILayout.ObjectField(interactablePrefab, typeof(GameObject), true);
        numberOfObjects = EditorGUILayout.IntSlider("Number of interactables", numberOfObjects, 0, 100);
        if (GUILayout.Button("Spawn")) {
            SpawnObjects();
            spawnWindow.Close();
        }
    }

    //Check the spawnable surface and Spawn the interactable objects we selected.
    private void SpawnObjects()
    {
        ground = GameObject.FindGameObjectWithTag("SpawnGround");
        maxX = ground.GetComponent<MeshCollider>().bounds.max.x;
        minX = ground.GetComponent<MeshCollider>().bounds.min.x;
        maxY = ground.GetComponent<MeshCollider>().bounds.max.y;
        minY = ground.GetComponent<MeshCollider>().bounds.min.y;
        if (numberOfObjects > 0)
        {
            for (int i = 0; i < numberOfObjects; i++)
            {
                Instantiate(interactablePrefab, new Vector3(Random.Range(minX, maxX), 1.85f, Random.Range(minY, maxY)), ground.transform.rotation, ground.transform.parent);
            }
        }
    }
}
