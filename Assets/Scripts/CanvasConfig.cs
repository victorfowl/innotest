﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Canvas Config", menuName = "InnoTest/CanvasConfig")]
public class CanvasConfig : ScriptableObject
{
    public Canvas sphereCanvas;
    public Image clickedImage;
    public Button closeButton;
    public Text text;
    public InputField inputField;
    public string textString;
}
